#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define Pi 3.14

int main()

{
    float degree, radiant;
    char litter;
    float size = 0;

    printf("Please, enter degree or radian! On form: 10D or 10R\n");
    if (scanf("%f%c", &size, &litter) == 2)
    {
        if (litter == 'D')
        {
            size = size*Pi / 180;
            printf("%3.0f\n", size);
        }
        else if (litter == 'R')
        {
            size = size * 180 / Pi;
            printf("%3.0f\n", size);
        }
        else puts("Error!");
       
    }
    else
        puts("Error!");

    return 0;
}